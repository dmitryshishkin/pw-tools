import React, { FC, ReactNode } from 'react';
import classes from './button.module.scss';
import classnames from 'classnames/bind';

const cx = classnames.bind(classes);

interface ButtonProps {
  children: ReactNode;
  onClick: () => void;
  size?: 'large' | 'medium';
}

export const Button: FC<ButtonProps> = ({
  onClick,
  children,
  size = 'medium',
}) => (
  <button className={cx('button', size)} onClick={onClick}>
    {children}
  </button>
);
