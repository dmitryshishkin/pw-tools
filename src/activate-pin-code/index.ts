import { getPinFromClipboard } from '../helpers/get-pin-from-clipboard';

export const activatePinCode = async () => {
  const form = document.querySelector(
    'form[action="/pin.php?do=activate"]',
  ) as HTMLFormElement;
  const filed = document.querySelector('#pin') as HTMLInputElement;

  if (filed && form && filed.value) {
    form.submit();
  } else if (filed && form) {
    const pinCode = await getPinFromClipboard();

    try {
      const isActivated = Boolean(
        new URL(window.location.href).searchParams.get('error'),
      );

      if (pinCode && !isActivated) {
        filed.value = pinCode;
        form.submit();
      }
    } catch (e) {}
  }
};
