import React from 'react';
import { render } from 'react-dom';
import { Transfer } from './transfer';

/**
 * Подключает функциональность перевода подарков в игру
 */
export const connectGiftTransfer = () => {
  const descriptionContainer = document.querySelector('.description-items');

  if (descriptionContainer) {
    const transferButtonContainer = document.createElement('div');
    descriptionContainer.append(transferButtonContainer);

    render(<Transfer />, transferButtonContainer);
  }
};
