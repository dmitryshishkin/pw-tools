import React, { FC, ReactNode } from 'react';
import classes from './classes.module.scss';
import classnames from 'classnames/bind';
import { Button } from '../components/button';

const cx = classnames.bind(classes);

export const Transfer = () => {
  const checkboxes = [...document.querySelectorAll('[name="cart_items[]"]')];

  const onCheckAll = () => {
    checkboxes.forEach(checkbox => {
      if (checkbox instanceof HTMLInputElement) {
        checkbox.checked = true;
      }
    });
  };

  const onUncheckAll = () => {
    checkboxes.forEach(checkbox => {
      if (checkbox instanceof HTMLInputElement) {
        checkbox.checked = false;
      }
    });
  };

  const onCheckDiona = () => {
    checkboxes.forEach(checkbox => {
      if (checkbox instanceof HTMLInputElement) {
        const itemMessage = checkbox
          .closest('tr')
          ?.querySelector('.img_item_cont span');
        const withDiona = !itemMessage?.textContent?.includes(
          'Только для серверов: Саргас, Скорпион, Гиперион, Фафнир, Диона',
        );

        checkbox.checked = withDiona;
      }
    });
  };

  // При входе всегда выделяем все
  onCheckAll();

  return (
    <div className={cx('wrapper')}>
      <Button onClick={onCheckAll}>Выбрать все подарки</Button>
      <Button onClick={onCheckDiona}>Выбрать доступные для Оберона</Button>
      <Button onClick={onUncheckAll}>Снять выбор</Button>
    </div>
  );
};
