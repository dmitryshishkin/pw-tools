export const getPinFromClipboard = async (): Promise<string | null> => {
  try {
    const clipboardText = await navigator.clipboard.readText();

    // Проверяем похож ли текст из буфера обмена на промокод
    if (typeof clipboardText === 'string') {
      const cleanText = clipboardText.replace(/[\s.,%]/g, '');

      if (/^([A-Z0-9]{3,20})$/.test(cleanText)) {
        return cleanText;
      }
    }

    return null;
  } catch (e) {
    return null;
  }
};
