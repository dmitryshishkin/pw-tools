import './styles.scss';
import { connectGiftTransfer } from './gitft-transfer';
import { activatePinCode } from './activate-pin-code';
import { connectStickyPinButton } from './sticky-pin-button';

const start = async () => {
  const isGiftTransferPage = location.pathname === '/promo_items.php';
  const isPinCodeFormPage = location.pathname.includes('pin');

  isGiftTransferPage && connectGiftTransfer();
  isPinCodeFormPage && (await activatePinCode());
  connectStickyPinButton();
};

(async function () {
  await start();
})();
