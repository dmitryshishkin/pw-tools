import React, { useEffect, useState } from 'react';
import classes from './classes.module.scss';
import classnames from 'classnames/bind';
import { Button } from '../components/button';
import { getPinFromClipboard } from '../helpers/get-pin-from-clipboard';

const cx = classnames.bind(classes);

export const StickyButton = () => {
  const [pinCode, setPinCode] = useState<null | string>(null);
  const updatePinCode = async () => {
    const newPinCode = await getPinFromClipboard();
    setPinCode(newPinCode);
  };

  // Получаем промокод при первой загрузке
  useEffect(() => {
    updatePinCode();
  }, []);

  useEffect(() => {
    document.addEventListener('copy', updatePinCode);

    return () => {
      document.removeEventListener('copy', updatePinCode);
    };
  }, [setPinCode]);

  const onClick = () => {
    window.location.assign(`/pin/${pinCode}`);
  };

  return pinCode ? (
    <div className={cx('wrapper')}>
      <Button onClick={onClick} size='large'>
        Активировать промокод {pinCode}
      </Button>
    </div>
  ) : null;
};
