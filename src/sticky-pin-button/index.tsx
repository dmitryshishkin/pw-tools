import React from 'react';
import { render } from 'react-dom';
import { StickyButton } from './sticky-button';

/**
 * Подключает функциональность перевода подарков в игру
 */
export const connectStickyPinButton = () => {
  const stickyPinButtonContainer = document.createElement('div');
  document.body.append(stickyPinButtonContainer);

  render(<StickyButton />, stickyPinButtonContainer);
};
